package manageBean;

import java.io.Serializable;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.NavigationHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import persistence.User;
import service.BasicOpsLocal;

@ManagedBean
@ViewScoped
public class SujetRatingBean implements Serializable {
	private static final long serialVersionUID = 1L;
	@EJB
	private BasicOpsLocal basicOpsLocal;
	private boolean formDisplayed = false;
	private boolean formDis1 = false ;
	private int idSujet;

	FacesContext facesContext = FacesContext.getCurrentInstance();
	HttpSession session = (HttpSession)facesContext.getExternalContext().getSession(true);
    private User currentUser = (User)session.getAttribute("currentSessionUser");
    Identity idu = new Identity();

	
	
	@PostConstruct
	public void init(){
		Map<String, String> params =FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		String parameterOne = params.get("id");
		idSujet = Integer.parseInt(parameterOne);
		}
	

public void goToComms(String id)
{ 
    session.setAttribute("sujet", 4); 
    FacesContext context = FacesContext.getCurrentInstance();
    NavigationHandler navigationHandler = context.getApplication().getNavigationHandler();
    navigationHandler.handleNavigation(context, null, "/pages/reclamation/listCommentaires.jsf?faces-redirect=true");
}

	public Identity getIdu() {
		return idu;
	}

	public void setIdu(Identity idu) {
		this.idu = idu;
	}

	public BasicOpsLocal getBasicOpsLocal() {
		return basicOpsLocal;
	}

	public void setBasicOpsLocal(BasicOpsLocal basicOpsLocal) {
		this.basicOpsLocal = basicOpsLocal;
	}

	public boolean isFormDisplayed() {
		return formDisplayed;
	}

	public void setFormDisplayed(boolean formDisplayed) {
		this.formDisplayed = formDisplayed;
	}

	public FacesContext getFacesContext() {
		return facesContext;
	}

	public void setFacesContext(FacesContext facesContext) {
		this.facesContext = facesContext;
	}

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	public User getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}

	public boolean isFormDis1() {
		return formDis1;
	}


	public void setFormDis1(boolean formDis1) {
		this.formDis1 = formDis1;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public int getIdSujet() {
		return idSujet;
	}

	public void setIdSujet(int idSujet) {
		this.idSujet = idSujet;
	}
	
	
	
	

}
