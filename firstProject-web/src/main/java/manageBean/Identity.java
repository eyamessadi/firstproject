package manageBean;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import persistence.Client;
import persistence.User;
import service.BasicOpsLocal;


@ManagedBean
@SessionScoped
public class Identity {
	@EJB
	private BasicOpsLocal basicOpsLocal;
	private Boolean loggedInAsManager = false;
	private User user = new User();
	private String role;
	
	private boolean isLogged = false;

	public String logout() {
		isLogged = false;
		System.out.println("isLogged : " + isLogged);

		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "/login?faces-redirect=true";
		
	}
	public String doLogin() {
		String navigateTo = "";
		User userLoggedIn = basicOpsLocal.loginUser(user.getLogin(), user.getPassword());

		if (userLoggedIn != null) {
			isLogged = true;
			user = userLoggedIn;
			if (userLoggedIn instanceof Client) {
				FacesContext facesContext = FacesContext.getCurrentInstance();
				 HttpSession session = (HttpSession)facesContext.getExternalContext().getSession(true);       
		          session.setAttribute("currentSessionUser",userLoggedIn); 
					System.out.println( userLoggedIn.getName());
					
if (userLoggedIn.getStatut().equals("valide")) {
				System.out.println("isLogged : " + isLogged);
				System.out.println("statut user : " + userLoggedIn.getStatut());

				navigateTo = "/pages/reclamation/home?faces-redirect=true";}
else {		
	System.out.println("statut user : " + userLoggedIn.getStatut());

	navigateTo = "login?faces-redirect=true";
}
			} else {
				loggedInAsManager = true;
				navigateTo = "/pages/manager/home?faces-redirect=true";
			
				}
		} else {
			loggedInAsManager = false;
			navigateTo = "erreur?faces-redirect=true";
		
			System.err.println("not");
		}
		return navigateTo;
	}
	
	public String doSign(){
		String navigateTo = "anObject";
		if(role.equals("client"))
		{
			Client client = new Client();
			client.setName(user.getName());
			client.setPassword(user.getPassword());
			client.setLogin(user.getLogin());
			basicOpsLocal.createUser(client);
		}
		return navigateTo;
		
		
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}


	public Boolean getLoggedInAsManager() {
		return loggedInAsManager;
	}
	public void setLoggedInAsManager(Boolean loggedInAsManager) {
		this.loggedInAsManager = loggedInAsManager;
	}
	public boolean isLogged() {
		return isLogged;
	}

	public void setLogged(boolean isLogged) {
		this.isLogged = isLogged;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public BasicOpsLocal getBasicOpsLocal() {
		return basicOpsLocal;
	}
	public void setBasicOpsLocal(BasicOpsLocal basicOpsLocal) {
		this.basicOpsLocal = basicOpsLocal;
	}
	
	
	
}
