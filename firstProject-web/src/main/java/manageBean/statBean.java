package manageBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpSession;

import persistence.Client;
import persistence.Reclamation;
import persistence.User;
import service.BasicOpsLocal;

@ManagedBean
@ViewScoped
public class statBean {
	private static final long serialVersionUID = 1L;
	@EJB
	private BasicOpsLocal basicOpsLocal;
	private boolean formDisplayed = false;
	private boolean formDis1 = false ;
	
	private int id;
	private String name;
	private String login;
	private String password;
	private String email;
	private Integer User_Reclame;
	private String description;
	private String type;
	private String degres;
	private String statut;
	private String statut_user;
	private int id_reclam;
	private List<User> users ;
	private List<String> rest; 
	private List<Reclamation> reclamations;
	private int reclamationsApp;
	private int reclamationsRej;
	FacesContext facesContext = FacesContext.getCurrentInstance();
	HttpSession session = (HttpSession)facesContext.getExternalContext().getSession(true);
    private User currentUser = (User)session.getAttribute("currentSessionUser");
    Identity idu = new Identity();
	private User user = new User();
	final String username = "consimitounsi@gmail.com";
    final String passwordEmail = "consomitounsi";
    private int k;
	
	
	@PostConstruct
	public void init(){	
				reclamationsApp = basicOpsLocal.findAllReclamationsApp();
				reclamationsRej = basicOpsLocal.findAllReclamationsRej();
		currentUser = (User)session.getAttribute("currentSessionUser");
		Map<String, String> params =FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		}
	
	public void getAllReclamation() {
		
		}
	
	public Identity getIdu() {
		return idu;
	}

	public void setIdu(Identity idu) {
		this.idu = idu;
	}

	public Integer getUser_Reclame() {
		return User_Reclame;
	}



	public void setUser_Reclame(Integer user_Reclame) {
		User_Reclame = user_Reclame;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public String getType() {
		return type;
	}



	public void setType(String type) {
		this.type = type;
	}



	public String getDegres() {
		return degres;
	}


	

	public String getStatut_user() {
		return statut_user;
	}

	public void setStatut_user(String statut_user) {
		this.statut_user = statut_user;
	}

	public void setDegres(String degres) {
		this.degres = degres;
	}



	public String getStatut() {
		return statut;
	}



	public void setStatut(String statut) {
		this.statut = statut;
	}



	public List<Reclamation> getReclamations() {
		return reclamations;
	}



	public void setReclamations(List<Reclamation> reclamations) {
		this.reclamations = reclamations;
	}



	public BasicOpsLocal getBasicOpsLocal() {
		return basicOpsLocal;
	}


	public void setBasicOpsLocal(BasicOpsLocal basicOpsLocal) {
		this.basicOpsLocal = basicOpsLocal;
	}


	public boolean isFormDisplayed() {
		return formDisplayed;
	}

	

	public int getK() {
		return k;
	}

	public void setK(int k) {
		this.k = k;
	}

	public void setFormDisplayed(boolean formDisplayed) {
		this.formDisplayed = formDisplayed;
	}
	
	


	public FacesContext getFacesContext() {
		return facesContext;
	}

	public void setFacesContext(FacesContext facesContext) {
		this.facesContext = facesContext;
	}

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	public User getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}

	public boolean isFormDis1() {
		return formDis1;
	}


	public void setFormDis1(boolean formDis1) {
		this.formDis1 = formDis1;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getLogin() {
		return login;
	}


	public void setLogin(String login) {
		this.login = login;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public List<User> getUsers() {
		return users;
	}


	public void setUsers(List<User> users) {
		this.users = users;
	}


	public List<String> getRest() {
		return rest;
	}


	public void setRest(List<String> rest) {
		this.rest = rest;
	}


	


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public int getReclamationsApp() {
		return reclamationsApp;
	}

	public void setReclamationsApp(int reclamationsApp) {
		this.reclamationsApp = reclamationsApp;
	}

	public int getReclamationsRej() {
		return reclamationsRej;
	}

	public void setReclamationsRej(int reclamationsRej) {
		this.reclamationsRej = reclamationsRej;
	}
	
	
	
	
}
