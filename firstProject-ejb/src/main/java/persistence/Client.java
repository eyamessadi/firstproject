package persistence;

import java.io.Serializable;
import javax.persistence.*;
import persistence.User;

/**
 * Entity implementation class for Entity: Client
 *
 */
@Entity
public class Client extends User implements Serializable {

	@Enumerated(EnumType.STRING)
	private Gender gender;
	private static final long serialVersionUID = 1L;

	public Client() {
		super();
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Client(String name, String login, String password,Gender gender) {
		super(name, login, password);
		this.gender=gender;
		// TODO Auto-generated constructor stub
	}

	public Client(String name, String login, String password, String email,Gender gender) {
		super(name, login, password, email);
		this.gender=gender;

		// TODO Auto-generated constructor stub
	}

	public Client(String name, String login, String password, String email, String statut, Gender gender) {
		super(name, login, password, email, statut);
		// TODO Auto-generated constructor stub
		this.gender=gender;

	}   
	
	
	
	
	

}
