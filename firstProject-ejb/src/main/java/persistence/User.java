package persistence;

import java.io.Serializable;
import java.lang.String;
import java.util.List;
import javax.persistence.*;



/**
 * Entity implementation class for Entity: User
 *
 */
@Entity
@Table(name="tUser")
public class User implements Serializable {

   
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String name;
	private String login;
	private String password;
	private String email;
	private String statut;
	@OneToMany(mappedBy="user")
	private List<Reclamation> js;

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getStatut() {
		return statut;
	}
	public void setStatut(String statut) {
		this.statut = statut;
	}
	public List<Reclamation> getJs() {
		return js;
	}
	public void setJs(List<Reclamation> js) {
		this.js = js;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public User(String name, String login, String password) {
		super();
		this.name = name;
		this.login = login;
		this.password = password;
	}
	
	public User(String name, String login, String password, String email, String statut) {
		super();
		this.name = name;
		this.login = login;
		this.password = password;
		this.email = email;
		this.statut = statut;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public User(String name, String login, String password, String email) {
		super();
		this.name = name;
		this.login = login;
		this.password = password;
		this.email = email;
	}

	private static final long serialVersionUID = 1L;

	public User() {
		super();
	}   
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
   
}
