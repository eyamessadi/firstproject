package persistence;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;



@Entity
public class Reclamation implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private Integer User_Reclame;
	private String description;
	private String type;
	private String degres;
	private String statut;
	@ManyToOne
	private User user;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getUser_Reclame() {
		return User_Reclame;
	}
	public void setUser_Reclame(Integer user_Reclame) {
		User_Reclame = user_Reclame;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDegres() {
		return degres;
	}
	public void setDegres(String degres) {
		this.degres = degres;
	}
	public String getStatut() {
		return statut;
	}
	public void setStatut(String statut) {
		this.statut = statut;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Reclamation() {
		super();
	}
	public Reclamation(Integer user_Reclame, String description, String type, String degres, String statut, User user) {
		super();
		User_Reclame = user_Reclame;
		this.description = description;
		this.type = type;
		this.degres = degres;
		this.statut = statut;
		this.user = user;
	}
	public Reclamation(Integer id, Integer user_Reclame, String description, String type, String degres, String statut,
			User user) {
		super();
		this.id = id;
		User_Reclame = user_Reclame;
		this.description = description;
		this.type = type;
		this.degres = degres;
		this.statut = statut;
		this.user = user;
	}
	public Reclamation(int i) {
		// TODO Auto-generated constructor stub
	}
	


	

}
