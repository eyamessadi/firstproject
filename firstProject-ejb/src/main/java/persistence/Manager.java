package persistence;

import java.io.Serializable;
import javax.persistence.*;
import persistence.User;

/**
 * Entity implementation class for Entity: Manager
 *
 */
@Entity

public class Manager extends User implements Serializable {

	
	private int experience;
	private static final long serialVersionUID = 1L;
	public Manager() {
		super();
	}
	public Manager(String name, String login, String password,int experience) {
		super(name, login, password);
		this.experience=experience;
		// TODO Auto-generated constructor stub
	}

	public int getExperience() {
		return this.experience;
	}

	public void setExperience(int experience) {
		this.experience = experience;
	}
   
}
