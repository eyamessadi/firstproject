package service;

import java.util.List;

import javax.ejb.Remote;

import persistence.Reclamation;
import persistence.User;

@Remote
public interface BasicOpsRemote {

	public void createUser(User user);
    public void updateUser(User user);
    public void deleteUser(User user);
    public User findUser(int id);
    public void removeUser(User user);
    public List<User> findAllUsers();
    public List<Reclamation> findAllReclamations();
    public void ajouterRecalamation(Reclamation reclamation);
    public void updateReclamation(Reclamation reclamation);
	public void removeReclamation(Reclamation reclamation);
	public Reclamation findReclamation(int id);
	public int  getnbpost();
    public int  getnbpostbypost(int statut);
    public User loginUser(String login,String mdp);
    public int findAllReclamationsApp();
    public int findAllReclamationsRej();
}
