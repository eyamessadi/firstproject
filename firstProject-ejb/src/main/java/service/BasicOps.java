package service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import persistence.*;
/**
 * Session Bean implementation class BasicOps
 */
@Stateless
public class BasicOps implements BasicOpsRemote, BasicOpsLocal {
	@PersistenceContext
	EntityManager em;
    /**
     * Default constructor. 
     */
    public BasicOps() {
        // TODO Auto-generated constructor stub
    }
    public void createUser(User user){
    	em.persist(user);
    }
    
    public void updateUser(User user){
    	em.merge(user);
    }
    public void deleteUser(User user){
    	em.remove(user);
    }
    public User findUser(int id){
    	return em.find(User.class, id);
    }
    public void removeUser(User user){
    	em.remove(findUser(user.getId()));
    }
    public void ajouterRecalamation(Reclamation reclamation){
    	em.persist(reclamation);
    }
    
    public void updateReclamation(Reclamation reclamation) {
    	em.merge(reclamation);

    }
	 public void removeReclamation(Reclamation reclamation) {
	    	em.remove(findReclamation(reclamation.getId()));

	 }
	 public Reclamation findReclamation(int id){
	    	return em.find(Reclamation.class, id);
	    }

	 public int  getnbpostbypost(int id){
			
			List<Reclamation> mm = em.createQuery("select c from Reclamation c where  c.User_Reclame=:id").setParameter("id", id).getResultList();
			return mm.size();

		}
	 public int  getnbpost(){
			
			Long mm =((Long) em.createQuery("select count(statut) from Reclamation r where r.statut=1 ").getSingleResult());
			int k =mm.intValue();
			return k;

		}

    public List<User> findAllUsers(){
    	String jpql="Select u from User u";
		Query query=em.createQuery(jpql);
		return query.getResultList();
		
    }
    public List<Reclamation> findAllReclamations(){
    	String jpql="Select u from Reclamation u";
		Query query=em.createQuery(jpql);
		return query.getResultList();
		
    }
    public User loginUser(String login,String mdp){
    	String jpql="Select u from User u where u.login=:login and u.password=:mdp";
		Query query=em.createQuery(jpql);
		query.setParameter("login", login);
		query.setParameter("mdp", mdp);
		return (User)query.getSingleResult();	
    }
	@Override
	public int findAllReclamationsApp() {
		List<Reclamation> mm = em.createQuery("select c from Reclamation c where  c.statut=:statut").setParameter("statut", "approuver").getResultList();
		return mm.size();
	}
	@Override
	public int findAllReclamationsRej() {
		List<Reclamation> mm = em.createQuery("select c from Reclamation c where  c.statut=:statut").setParameter("statut", "rejeter").getResultList();
		return mm.size();
	}
    

    
}
