package service;

import java.util.List;
import javax.ejb.Local;
import persistence.Reclamation;
import persistence.User;

@Local
public interface BasicOpsLocal {
	public void createUser(User user);
    public void updateUser(User user);
    public void deleteUser(User user);
    public User findUser(int id);
    public void removeUser(User user);
    public void ajouterRecalamation(Reclamation reclamation);
    public void updateReclamation(Reclamation reclamation);
	public void removeReclamation(Reclamation reclamation);
	public Reclamation findReclamation(int id);
    public List<User> findAllUsers();
    public int  getnbpost();
	public int  getnbpostbypost(int statut);
    public List<Reclamation> findAllReclamations();
    public int findAllReclamationsApp();
    public int findAllReclamationsRej();

    public User loginUser(String login,String mdp);
}
